CREATE SERVER geomodel_jupiter_fdw
        FOREIGN DATA WRAPPER postgres_fdw
        OPTIONS (host 'localhost', port '5433', dbname 'fohm_points');
CREATE USER MAPPING FOR CURRENT_USER
  SERVER geomodel_jupiter_fdw
  OPTIONS (user 'postgres', password 'GKhuk35i');
CREATE SCHEMA geomodel_fdw;
IMPORT FOREIGN SCHEMA fohm
  FROM SERVER geomodel_jupiter_fdw
  INTO geomodel_fdw;
 
select *
from geomodel_fdw.kommune_2007

drop schema jupiter_fdw cascade;

CREATE SERVER jupiter_geomodel_fdw
        FOREIGN DATA WRAPPER postgres_fdw
        OPTIONS (host 'localhost', port '5433', dbname 'pcjupiterxl');
CREATE USER MAPPING FOR CURRENT_USER
  SERVER jupiter_geomodel_fdw
  OPTIONS (user 'postgres', password 'GKhuk35i');
CREATE SCHEMA jupiter_fdw;
IMPORT FOREIGN SCHEMA jupiter
  FROM SERVER jupiter_geomodel_fdw
  INTO jupiter_fdw;
IMPORT FOREIGN SCHEMA sm_jupiter
  FROM SERVER jupiter_geomodel_fdw
  INTO jupiter_fdw;