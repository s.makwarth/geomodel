drop table fohm.grid_odder;
create table fohm.grid_odder as 
	(
		with 
			kom_series as 
				(
					select 
						generate_series(floor(st_xmin(geom))::int, ceil(st_xmax(geom))::int, 10) as x,
						generate_series(floor(st_ymin(geom))::int, ceil(st_ymax(geom))::int, 10) as y,
						generate_series(-300, 200, 10) as z
					from fohm.kommune_2007 k
					where navn = 'Odder'
				),
			kom_grid as
				(
					select 
						ST_SetSRID(ST_MakePoint(k1.x, k2.y, k3.z), 25832) as geom_grid
					from kom_series k1
					cross join kom_series k2
					cross join kom_series k3
					where k1.x is not null
						and k2.y is not null
						and k3.z is not null
				)
		select 
			uuid_generate_v5(uuid_ns_url(), st_astext(kg.geom_grid)) as uuid,
			tmp.navn, 
			kg.geom_grid as geom
		from kom_grid kg
		inner join
			(
				select 
					navn,
					geom as geom_munci
				from fohm.kommune_2007 k
					where navn = 'Odder'
			) tmp on st_intersects(kg.geom_grid, tmp.geom_munci)
	)
;

drop table fohm.grid_odder_10;
create table fohm.grid_odder_10 as 
	(
		with 
			kom_series as 
				(
					select 
						generate_series(floor(st_xmin(geom))::int, ceil(st_xmax(geom))::int, 10) as x,
						generate_series(floor(st_ymin(geom))::int, ceil(st_ymax(geom))::int, 10) as y
						--generate_series(-300, 200, 10) as z
					from fohm.kommune_2007 k
					where navn = 'Odder'
				),
			xy_points as 
				(
					select 
						 k1.x,
						 k2.y
					from kom_series k1
					cross join kom_series k2
					--cross join kom_series k3
					where k1.x is not null
						and k2.y is not null
				)
		select 
			xy.*
		from xy_points xy
		inner join fohm.kommune_2007 k on st_intersects(ST_SetSRID(ST_MakePoint(xy.x, xy.y), 25832), k.geom)
			--and k3.z is not null
	)
;
				

CREATE INDEX grid_odder_geom_idx
  ON fohm.grid_odder
  USING GIST (geom);
 
 alter table fohm.grid_odder
	ADD PRIMARY key (uuid);

drop table fohm.lithsamp_odder;
create table fohm.lithsamp_odder as (
select *
from jupiter_fdw.lithsamp_odder
);

alter table fohm.lithsamp_odder
	ADD PRIMARY key (uuid);

CREATE INDEX lithsamp_odder_geom_idx
	ON fohm.lithsamp_odder
	USING GIST (geom_lith);
 
CREATE INDEX lithsamp_odder_idx
	ON fohm.lithsamp_odder
	USING BTREE (boreholeid);

truncate fohm.tagged_points;
insert into fohm.tagged_points
	(
		with 
		 	tmp as 
		 		(
					 select lo.*
					 from fohm.lithsamp_odder lo
					 left join fohm.tagged_points tp on tp.uuid_lo = lo.uuid 
					 where tp.uuid is null
					 	and char_length(lo.rocksymbol) > 1 
		 		),
		 	tmp2 as
		 		(
			 		select
						lo.uuid as uuid_lo,
						go.uuid,
						ST_3DDistance(go.geom, lo.geom_lith) as dist
					from tmp lo, fohm.grid_odder go
		 		)
		SELECT uuid, uuid_lo, dist 
		from tmp2 
		where dist <= 100
	)
;

create table fohm.grid_odder_tagged as 
	(
		with tmp as 
			(
				select 
					go.uuid, lo.rocksymbol, 
					lo.geom_lith, tp.dist, 
					go.geom 
				from fohm.grid_odder go 
				left join fohm.tagged_points tp  on go.uuid = tp.uuid
				left join fohm.lithsamp_odder lo on lo.uuid = tp.uuid_lo
				order by go.uuid, tp.dist 
			)
		select 
			tmp.uuid, 
			mode() WITHIN GROUP (ORDER BY tmp.rocksymbol) AS pri_rs, 
			case when string_agg(tmp.rocksymbol, ', ') is null then 0 else count(*) end as count_rs, 
			string_agg(tmp.rocksymbol, ', ') as all_rs,
			geom
		FROM tmp
		group by tmp.uuid, geom
	)
;


drop table fohm.tagged_points_2;
create table fohm.tagged_points_2 as 
	(
		with 
		 	tmp as 
		 		(
					 select lo.*
					 from fohm.lithsamp_edit lo
					 --left join fohm.tagged_points tp on tp.uuid_lo = lo.uuid 
					 --where char_length(lo.rocksymbol) > 1 
		 		),
		 	tmp2 as
		 		(
			 		select
						lo.uuid as uuid_lo,
						go.uuid,
						ST_3DDistance(go.geom, lo.geom_lith) as dist
					from tmp lo, fohm.grid_odder go
		 		)
		select --distinct on (uuid)
			uuid, uuid_lo,
			dist
		from tmp2 
		where (uuid, dist) in
			(
				select uuid, min(dist)
				from tmp2
				group by uuid
			)
	)
;

drop table fohm.dist_points;
create table fohm.dist_points as 
	(
		with 
			tmp1 as
				(
					select *
					from fohm.lithsamp_edit ls
					--limit 100
				),
		 	tmp2 as 
		 		(
			 		select
						ls.uuid as uuid_ls,
						go.uuid,
						ST_3DDistance(go.geom, ls.geom_lith) as dist
					from tmp1 ls, fohm.grid_odder go
				),
			tmp3 as 
				(
					SELECT 
						*, 
						ROW_NUMBER() OVER (PARTITION BY uuid order by dist) AS row_num 
					FROM tmp2
				)
		select
			uuid, 
			uuid_ls,
			dist
		from tmp3
		where 
			(
				row_num < 3 and dist < 100
				or 
				row_num < 1 and dist < 5000
			)
	)
;

drop table fohm.grid_odder_tagged_2;
create table fohm.grid_odder_tagged_2 as 
	(
		select distinct 
			go.uuid, lo.rocksymbol,
			st_z(go.geom) as z,
			tp.dist, go.geom 
		from fohm.grid_odder go 
		left join fohm.tagged_points_2 tp  on go.uuid = tp.uuid
		left join fohm.lithsamp_odder lo on lo.uuid = tp.uuid_lo
		order by go.uuid, tp.dist 
	)
;

select min(got.dist), max(dist)
from fohm.grid_odder_tagged_2 got


select 
	lo.*, right(lo.rocksymbol, 1) as rocksymbol_simple
from fohm.lithsamp_odder lo 
where right(lo.rocksymbol, 1) not in ('x', 'b', 'u', 'o', 'm')