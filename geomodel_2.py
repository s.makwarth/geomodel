import pandas as pd
import glob
import os
from itertools import product
import re
import numpy as np
from sqlalchemy import create_engine
from shapely.geometry import Point
from geoalchemy2 import Geometry

path = r'C:\Users\Ejer\Downloads\fohm_grids (1)'
all_files = glob.glob(f'{path}/*.xyz')

df_list = []

for filename in all_files:
    base = os.path.basename(filename)
    layer_name = os.path.splitext(base)[0]
    layer_id = int(re.findall(r'\d+', layer_name)[0])

    df = pd.read_csv(filename, sep=' ')
    df.columns = ['x', 'y', 'z']
    df['layer_name'] = layer_name
    df['layer_id'] = layer_id
    df_list.append(df)

df_merge = pd.concat(df_list, axis=0, ignore_index=True)
print(df_merge)

x_min = int(df_merge['x'].min())
x_max = int(df_merge['x'].max())
y_min = int(df_merge['y'].min())
y_max = int(df_merge['y'].max())
# z_max = int(df_merge['z'].max())
# z_min = int(df_merge['z'].min())
z_max = 200
z_min = -4000

x = np.arange(x_min, x_max, 100)
y = np.arange(y_min, y_max, 100)
z = np.arange(z_min, z_max, 100)
# print(x)
# print(y)
# print(z)
# mesh = np.empty((x, y, z))
# print(mesh)

# test = np.array([xx, yy, z]).reshape(3, -1).T
# print(test)
# print(z)
# df_grid = pd.DataFrame(index=x, columns=y)
# arrays = [x, y, z]
# df_grid = pd.MultiIndex.from_arrays(arrays, names=('x', 'y', 'z'))
xyz =list(product(x, y, z))
c = []
for i in xyz:
    c.append([Point(i[0], i[1], i[2])])
# df_grid = pd.DataFrame(list(product(x, y, z)), columns=['x', 'y', 'z'])
df_grid = pd.DataFrame(i[2], columns=['z'], index=c)
df_grid['lithologi'] = np.nan
print(df_grid)
exit()
# combined = list(product(x, y, z))
# print(combined)
schemaname = 'juelsminde'
tablename = 'grid'
index = False
if_exists = 'replace'
usr = 'postgres'
pw = 'GKhuk35i'
host = 'localhost'
port = '5433'
database = 'fohm_points'
pg_conn = f"postgresql+psycopg2://{usr}:{pw}@{host}:{port}/{database}"
engine = create_engine(pg_conn, use_batch_mode=True)
geom_type = 'Point'
srid = 25832

# df_grid.to_sql(name=tablename, con=engine, schema=schemaname, if_exists=if_exists, index=index,
#            dtype={'geom': Geometry(geom_type, srid=srid)})

tablename = 'fohm_layers'
df_merge.to_sql(name=tablename, con=engine, schema=schemaname, if_exists=if_exists, index=index)

