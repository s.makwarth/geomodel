drop table sm_jupiter.lithsamp_odder;
create table sm_jupiter.lithsamp_odder as 
	(
		with tmp as 
			(
				select
					boreholeid, ls.rocksymbol, ls.totaldescr, 
					coalesce(ls.sampledep, (bottom-top)/2) as sampledep_est,
					b.geom,
					ST_SetSRID(ST_MakePoint(ST_X(b.geom), ST_Y(b.geom), b.elevation-coalesce(ls.sampledep, (bottom-top)/2)), 25832) as geom_lith
				from jupiter.lithsamp ls
				inner join jupiter.borehole b using (boreholeid)
				inner join geomodel_fdw.kommune_2007 k on st_intersects(b.geom, k.geom)
				where k.navn = 'Odder'
					and coalesce(ls.sampledep, (bottom-top)/2) is not null
					and COALESCE(lower(ls.rocksymbol),'x') NOT IN ('x', 'b', 'u')
			)
		select 
		uuid_generate_v4() as uuid,
		tmp.*
		from tmp
	)
;

