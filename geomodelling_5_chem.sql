with 
	geom_pkt as 
		(
			select 
				*
			FROM temp_simak.grid_odder_1000x1000x100 
			LIMIT 2
				--1 AS grid_id,
				--ST_SetSRID(ST_MakePoint(571597, 6199800, 100), 25832) as geom,
				--ST_SetSRID(ST_MakePoint(571597, 6199800), 25832) AS geom_2d
			
		 ),
	geom_chem as 
		(
			select DISTINCT 
				*,
				ST_3DDistance(wt.geom, gp.geom) AS dist,
				ST_Distance(wt.geom_2d, gp.geom_2d) AS dist_2d
			from temp_simak.watertype wt
			inner join geom_pkt gp on ST_3DDWithin(wt.geom, gp.geom, 5000)
			WHERE abs(st_z(wt.geom) - st_z(gp.geom)) / ST_Distance(wt.geom_2d, gp.geom_2d) < 0.1
				OR  abs(st_z(wt.geom) - st_z(gp.geom)) < 100
		),
	chem_inv_dist as 
		(
			select 
				gc.*,
				1/(dist^4) as inv_dist,
				1/(dist_2d^4) as inv_dist_2d
			from geom_chem gc
		),
	chem_inv_dist_all as 
		(
			select 
				uuid,
				sum(inv_dist) as inv_dist_sum,
				sum(inv_dist) as inv_dist_sum_2d
			from chem_inv_dist
			GROUP BY uuid
		)
select DISTINCT
	uuid,
	vandtype,
	min(dist) dist_min, 
	min(dist_2d) dist_min_2d, 
	count(*) AS num_samps,
	sum(inv_dist) inv_dist_sum,
	sum(inv_dist_2d) inv_dist_sum_2d,
	round(sum(inv_dist)/inv_dist_sum * 100) as inv_dist_sum_relative,
	round(sum(inv_dist_2d)/inv_dist_sum_2d * 100) as inv_dist_sum_relative_2d
from chem_inv_dist d
INNER JOIN chem_inv_dist_all da USING (uuid)
group by uuid, vandtype, inv_dist_sum, inv_dist_sum_2d
order by uuid, round(sum(inv_dist)/inv_dist_sum * 100) desc nulls last 
;

