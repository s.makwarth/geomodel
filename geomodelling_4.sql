create table fohm.lithsamp as (
select *
from jupiter_fdw.lithsamp l 
)
;

create table fohm.lithsamp_edit as 
	(
		select 
			uuid_generate_v4() as uuid,
			b.boreholeid, b.boreholeno, 
			ls.rocksymbol, ls.minorcomps, 
			right(ls.rocksymbol, 1) as rocksymbol_simple,
			ls.top, ls.bottom, 
			ls.totaldescr, ls.sampledep, 
			b.elevation,
			coalesce(ls.sampledep, (bottom-top)/2) as sampledep_est,
			b.geom, 
			ST_SetSRID(ST_MakePoint(ST_X(b.geom), ST_Y(b.geom), b.elevation-coalesce(ls.sampledep, (bottom-top)/2)), 25832) as geom_lith
		from fohm.borehole b 
		inner join 
			(
				select st_buffer(geom, 3000) as geom
				from fohm.kommune_2007
				where navn = 'Odder'
			) k on st_intersects(b.geom, k.geom)
		inner join fohm.lithsamp ls using (boreholeid)
		where coalesce(ls.sampledep, (bottom-top)/2) is not null
			and COALESCE(lower(ls.rocksymbol),'x') NOT IN ('x', 'b', 'u', 'm', 'o')
	)
;

CREATE INDEX lithsamp_edit_geom_idx
	ON fohm.lithsamp_edit 
	USING GIST (geom);
 
CREATE index lithsamp_edit_idx
	ON fohm.lithsamp_edit
	USING BTREE (boreholeid);
	
CREATE INDEX borehole_idx
	ON fohm.borehole 
	USING BTREE (boreholeid);
	
CREATE INDEX kommune_geom_idx
	ON fohm.kommune_2007 
	USING GIST (geom);