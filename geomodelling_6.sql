drop table if exists temp_simak.lithsamps;
create table temp_simak.lithsamps as 
	(
		with 
			boreholes as 
				(
					select 
						b.boreholeid,
						b.elevation,
						b.geom
					from jupiter.borehole b 
--					inner join 
--						(
--							select geom as geom
--							from simak.kommune_2007
--							where navn = 'Odder'
--						) k on ST_DWithin(b.geom, k.geom, 3000)
					where b.elevation is not null
				)
		SELECT distinct 
			boreholeid, ls.guid,
			case when char_length(ls.rocksymbol) > 1 then ls.rocksymbol else null end as rocksymbol_complex,
			right(ls.rocksymbol, 1) as rocksymbol_simple, 
			ls.totaldescr, ls.minorcomps,
			st_x(b.geom) as x, st_y(b.geom) as y,
			b.elevation - coalesce(ls.sampledep, (bottom+top)/2, top) as z,
			ST_SetSRID(ST_MakePoint(st_x(b.geom), st_y(b.geom), b.elevation - coalesce(ls.sampledep, (bottom+top)/2, top)), 25832) as geom,
			b.geom AS geom_2d
		from boreholes b
		inner join jupiter.lithsamp ls using (boreholeid)
		where (b.elevation - coalesce(ls.sampledep, (bottom+top)/2, top)) is not null
			and right(ls.rocksymbol, 1) not in ('x', 'b', 'm', 'v', 'u', 'o')
	)
;

CREATE INDEX lithsamp_geom_idx
	ON temp_simak.lithsamps
	USING GIST (geom);
 
CREATE index lithsamp_rocksymbol_idx
	ON temp_simak.lithsamps
	USING BTREE (rocksymbol_simple);

CREATE index lithsamp_borid_idx
	ON temp_simak.lithsamps
	USING BTREE (boreholeid);

drop table if exists temp_simak.watertype;
create table temp_simak.watertype as
	(
		with 
			boreholes as 
				(
					select 
						boreholeid,
						guid_intake,
						st_x(b.geom) as x, st_y(b.geom) as y,
						b.elevation - coalesce((s.bottom+s.top)/2, s.top) as z
					from jupiter.borehole b 
--					inner join 
--						(
--							select geom as geom
--							from simak.kommune_2007
--							where navn = 'Odder'
--						) k on ST_DWithin(b.geom, k.geom, 3000)
					inner join jupiter.screen s using (boreholeid)
					where (b.elevation - coalesce((s.bottom+s.top)/2, s.top)) is not null 
				),
			jsons as
				(
					select 
						b.boreholeid,
						guid_intake,
						x, y, z, sampleid,
						jsonb_object_agg(gca.compoundno, gca.amount order by gca.compoundno) as amounts
					from boreholes b
					inner join jupiter.grwchemsample gcs using (guid_intake)
					inner join jupiter.grwchemanalysis gca using (sampleid)
					group by b.boreholeid, guid_intake, x, y, z, sampleid
				),
			pivot as 
				(
					select 
						boreholeid,
						guid_intake, 
						x, y, z, sampleid, 
						amounts ->> '1176' as nitrat,
						amounts ->> '2041' as jern,
						amounts ->> '2142' as sulfat,
						amounts ->> '251' as oxygen_indhold,
						amounts ->> '2086' as mangan,
						amounts ->> '1014' as ammonium_n
					from jsons
				),
			pivot_notnull as
				(
					select
						boreholeid,
						guid_intake, 
						x, y, z, sampleid,
						nitrat::numeric,
						sulfat::numeric,
						jern::numeric,
						oxygen_indhold::numeric,
						mangan::numeric,
						ammonium_n::numeric
					from pivot
				),
			watertype as
				(
					select 
						boreholeid,
						guid_intake, 
						x, y, z, sampleid,
						CASE
					        WHEN 
					            ic.nitrat > 1
								AND ic.jern >= 0.2 
					            THEN 'X'::text
							WHEN 
					            ic.nitrat > 1 
					            AND ic.jern < 0.2
								AND ic.jern >=0
								AND (ic.oxygen_indhold IS NULL)-- OR ic.oxygen_indhold <= 0)
					            THEN 'AB'::text
							WHEN 
					            ic.nitrat > 1 
					            AND ic.jern < 0.2
								AND ic.jern >= 0
					            AND ic.oxygen_indhold < 1 
					            THEN 'B'::text
							WHEN 
					            ic.nitrat > 1 
					            AND ic.jern < 0.2
								AND ic.jern >= 0
					            AND ic.oxygen_indhold >= 1 
					            THEN 'A'::text
							WHEN
								ic.nitrat <= 1
								AND ic.nitrat >= 0
					            AND ic.jern < 0.2
								AND ic.jern >= 0
								AND ic.oxygen_indhold >= 1
								AND ic.mangan < 0.01
								AND ic.mangan >= 0
								AND ic.ammonium_n < 0.01
								AND ic.ammonium_n >= 0
					            THEN 'AY'::text
					        WHEN 
					            ic.nitrat <= 1
								AND ic.nitrat >= 0
					            AND ic.jern < 0.2
								AND ic.jern >= 0
					            THEN 'Y'::text
					        WHEN 
					            ic.nitrat <= 1
								AND ic.nitrat >= 0
					            AND ic.jern >= 0.2
					            AND ic.sulfat < 20
								AND ic.sulfat >= 0
					            THEN 'D'::text
					        WHEN 
					            ic.nitrat <= 1
								AND ic.nitrat >= 0
					            AND ic.jern >= 0.2
					            AND ic.sulfat >= 20 AND ic.sulfat < 70 
								THEN 'C1'::text
							WHEN
								ic.nitrat <= 1
								AND ic.nitrat >= 0
					            AND ic.jern >= 0.2
					            AND ic.sulfat >= 70  
					            THEN 'C2'::text
					        ELSE NULL
							END AS vandtype,
						nitrat,
						sulfat,
						jern,
						oxygen_indhold,
						mangan,
						ammonium_n
					from pivot_notnull ic
				)
		select distinct on (guid_intake)
			wt.*,
			ST_SetSRID(ST_MakePoint(wt.x, wt.y, wt.z), 25832) as geom,
			ST_SetSRID(ST_MakePoint(wt.x, wt.y), 25832) AS geom_2d
		from watertype wt
		inner join jupiter.grwchemsample gcs using (guid_intake)
		where wt.vandtype is not null
		order by guid_intake, gcs.sampledate desc nulls last 
	)
;

CREATE INDEX watertype_geom_idx
	ON temp_simak.watertype
	USING GIST (geom);
 
CREATE index watertype_watertype_idx
	ON temp_simak.watertype
	USING BTREE (vandtype);

CREATE index watertype_borid_idx
	ON temp_simak.watertype
	USING BTREE (boreholeid);


drop table if exists simak.waterlevel;
create table simak.waterlevel as 
	(
		with 
			boreholes as 
				(
					select 
						boreholeid,
						guid_intake,
						st_x(b.geom) as x, st_y(b.geom) as y,
						b.elevation - coalesce((s.bottom+s.top)/2, s.top) as z
					from jupiter.borehole b 
					inner join 
						(
							select geom as geom
							from simak.kommune_2007
							where navn = 'Odder'
						) k on ST_DWithin(b.geom, k.geom, 3000)
					inner join jupiter.screen s using (boreholeid)
					where (b.elevation - coalesce((s.bottom+s.top)/2, s.top)) is not null 
				)
		select distinct on (guid_intake)
			b.boreholeid,
			b.guid_intake,
			wl.watlevmsl,
			b.x, b.y, b.z
		from boreholes b
		inner join jupiter.watlevel wl using (guid_intake)
		where wl.watlevmsl is not null
			and abs(wl.watlevmsl) < 200
		order by guid_intake, timeofmeas desc nulls last
	)
;

drop table if exists simak.wl_chem_combined;
create table simak.wl_chem_combined as 
	(
		select distinct
			coalesce(wl.guid_intake, wt.guid_intake) as guid_intake,
			coalesce(wl.boreholeid, wt.boreholeid) as boreholeid,
			coalesce(wl.x, wt.x) as x,
			coalesce(wl.y, wt.y) as y,
			coalesce(wl.z, wt.z) as z,
			wl.watlevmsl,
			wt.vandtype,
			ST_SetSRID(ST_MakePoint(coalesce(wl.x, wt.x), coalesce(wl.y, wt.y), coalesce(wl.z, wt.z)), 25832) as geom
		from simak.waterlevel wl
		full outer join simak.watertype wt using (guid_intake)
	)
;
with tmp as (
select 
	guid_intake, 
	round(avg(watlevmsl),2) as wl_ele_avg,
	round((PERCENTILE_CONT(0.5) WITHIN GROUP(ORDER BY watlevmsl))::numeric,2) as wl_ele_med
from jupiter.watlevel w 
where watlevmsl is not null
group by guid_intake 
)
select *
from tmp
where wl_ele_avg - wl_ele_med > 10
--having round(avg(watlevmsl),2)  - round((PERCENTILE_CONT(0.5) WITHIN GROUP(ORDER BY watlevmsl))::numeric,2) > 10